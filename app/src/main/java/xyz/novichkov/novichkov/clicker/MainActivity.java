package xyz.novichkov.novichkov.clicker;

import android.app.Activity;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    private int mCurrentCounter = 0;
    private int mRecord;
    private Button mBtn;
    private MediaPlayer mp;
    private TextView m_recordTv;
    public static final String APP_PREFERENCES = "mysettings";
    public static final String APP_PREFERENCES_COUNTER = "counter";
    private SharedPreferences mSettings;


    private View.OnClickListener m_BtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mCurrentCounter = mCurrentCounter + 1;
            mBtn.setText(String.valueOf(mCurrentCounter));
            if (mp.isPlaying())
            {
                mp.pause();
            }
            else
            {
                mp.start();
            }

            if (mCurrentCounter > mRecord)
            {
                m_recordTv.setText("Рекорд составляет "
                        + mCurrentCounter + " нажатий");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBtn = findViewById(R.id.push_button);
        mBtn.setOnClickListener(m_BtnListener);
        mp = MediaPlayer.create(this, R.raw.click);
        m_recordTv = findViewById(R.id.record_tv);

        mSettings = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);

    }

    @Override
    protected void onPause() {
        super.onPause();
        // Запоминаем данные
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putInt(APP_PREFERENCES_COUNTER, mCurrentCounter);
        editor.apply();
    }


    @Override
    protected void onStop() {
        super.onPause();
        // Запоминаем данные
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putInt(APP_PREFERENCES_COUNTER, mCurrentCounter);
        editor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mSettings.contains(APP_PREFERENCES_COUNTER)) {
            // Получаем число из настроекd
            mRecord = mSettings.getInt(APP_PREFERENCES_COUNTER, 0);
            // Выводим на экран данные из настроек


            m_recordTv.setText("Рекорд составляет "
                    + mRecord + " нажатий");
        }
    }

}
